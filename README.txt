README
Lab2 Mingheng Song 5101109067, Yiming Hao 5111019027

Establish a server on a VM of my PC(net bridge), which IP address is 192.168.199.185.
Two clients are run in VM of a Mac(net bridge), which IP address is 192.168.199.174.

The screenshots shows that after run clients with argument of IP address of the server, two clients concurrently connects to the server. By requiring two different files, two clients successfully received both files. 
MD5 values prove the integrity of the transferred file.

Use 	gcc mServer.c -pthread -o mServer
	gcc mClient.c -o mClient
to compile.

GitLab repository: https://gitlab.com/tk1114632/multiServer.git